# keyboard_latency

This daemon works around an issue on the Dell XPS 13 9333 laptop where keyboard lag is experienced when SATA link power savings are enabled. The daemon monitors for keyboard use and then keeps the CPU from transitioning to package c-states lower than PC3 while the keyboard is in use. 

### Usage

This daemon must be run as root. Either use the included systemd service file to start, or start manually from the commandline:

```$ sudo ./keyboard_latency <keyboard device> <timeout> <cpu latency> <daemonize>```

Where:

```keyboard device``` is the keyboard found at /dev/input/eventX

```daemonize``` is 1 or 0, whether to fork to background (1) or run in foreground (0)

```timeout``` and ```cpu latency``` parameters can be adjusted, I've found that 500 and 100 (respectively) work well for me.


### Installation

```
make
```

```keyboard_latency.service``` can be installed and used by systemd to start daemon automatically.


#### Arch Linux

Install [keyboard_latency from AUR](https://aur.archlinux.org/packages/keyboard_latency/).

### Credit
This daemon was created by Gabriele Mazzotta, who is also author of [this excellent resource for running Linux on the Dell XPS 13 9333 laptop.](https://xps13-9333.appspot.com) I am merely redistributing the application so others can benefit :)
