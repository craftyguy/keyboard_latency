/*
Copyright (C) 2016 Gabriele Mazzotta <gabriele.mzt@gmail.com>

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/




#include <linux/input.h>

#include <errno.h>
#include <fcntl.h>
#include <getopt.h>
#include <poll.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#define CPU_DMA_LATENCY "/dev/cpu_dma_latency"

#define DEFAULT_INACTIVE_TIMEOUT 500
#define DEFAULT_MAX_CPU_LATENCY 100

static int keybord_event_fd = -1;
static int pm_qos_fd = -1;
static int max_latency = DEFAULT_MAX_CPU_LATENCY;

static void set_idle()
{
	if (pm_qos_fd < 0)
		return;

	printf("Idle\n");
	close(pm_qos_fd);
	pm_qos_fd = -1;
}

static void set_active()
{
	if (pm_qos_fd >= 0)
		return;

	printf("Active\n");
	pm_qos_fd = open(CPU_DMA_LATENCY, O_RDWR);
	if (pm_qos_fd < 0) {
		printf("Could not open '%s': %s\n",
		       CPU_DMA_LATENCY, strerror(errno));
		return;
	}

	write(pm_qos_fd, &max_latency, sizeof(max_latency));
}

static void exit_program(int arg)
{
	close(keybord_event_fd);
	if (pm_qos_fd > 0)
		close(pm_qos_fd);
	exit(0);
}

int main(int argc, char *argv[])
{
	struct pollfd keyboard_poll;
	struct input_event ev;
	int timeout = DEFAULT_INACTIVE_TIMEOUT;
	int daemonize = 0;
	int ret;


	if (argc < 2) {
		printf("Usage: %s /dev/input/eventX [TIMEOUT] [CPU_LATENCY] [DAEMON]\n",
		       argv[0]);
		exit(EXIT_FAILURE);
	}

	if (argc > 2)
		timeout = atoi(argv[2]);
	if (argc > 3)
		max_latency = atoi(argv[3]);
	if (argc > 4)
		daemonize = atoi(argv[4]);

	keybord_event_fd = open(argv[1], O_RDONLY);
	if (keybord_event_fd == -1) {
		fprintf(stderr, "Could not open '%s': %s\n",
			argv[1], strerror(errno));
		exit(EXIT_FAILURE);
	}

	signal(SIGINT, exit_program);

	if (daemonize)
		daemon(0, 0);

	keyboard_poll.fd = keybord_event_fd;
	keyboard_poll.events = POLLIN;
	keyboard_poll.revents = 0;

	for(;;) {
		int n = poll(&keyboard_poll, 1, timeout);
		if (n > 0) {
			set_active();
			read(keyboard_poll.fd, &ev, sizeof(ev));
		} else if (n == 0) {
			set_idle();
		}
	}

	close(keybord_event_fd);
	if (pm_qos_fd > 0)
		close(pm_qos_fd);

	return 0;
}
